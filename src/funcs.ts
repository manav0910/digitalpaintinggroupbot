import { TelegrafContext } from "telegraf/typings/context";
import { ChatMember, User } from "telegraf/typings/telegram-types";
import * as data from './data/command_data.json'
import { Markup } from "telegraf";

const sendToLogChannel = async (ctx: TelegrafContext, message: string, logType: string) => {
    try {
        await ctx.tg.sendMessage(
            process.env.dev_id!,
            `${message}`,
            { parse_mode: 'Markdown' }
        );
    } catch (err) {
        console.log(`\`${err}\` \nError sending to log channel, couldn't send \n${logType}: \n\t${message}\n`)
    }
}

const isAdmin = (data: ChatMember[], id: number | undefined) => {
    return data.some(adm => adm.user.id == id);
};

const getName = (user: User) => {
    return `${(user.first_name ? user.first_name : user.first_name + user.last_name)}`
}

const command = (cmd: string, text: string) => async (ctx: TelegrafContext) => {
    try {
        if (ctx.chat?.type == "private") {
            await ctx.replyWithMarkdown(text, { disable_web_page_preview: true })
        } else {
            await ctx.replyWithMarkdown(`/${cmd} works in private mode, go [here](${process.env.link_bot})`, { disable_web_page_preview: true });
            await ctx.deleteMessage(ctx.message?.message_id);
        }
    } catch (err) {
        sendError(ctx, err);
    }
}

export const sendError = (ctx: TelegrafContext, err: unknown) => {
    const pretext = ctx ? `*User error with context:*\n\`${JSON.stringify(ctx.message, null, 2)}\`` : `*Unknown err*`;
    sendToLogChannel(ctx, `${pretext}\n*FULL ERROR:* \n\`${JSON.stringify(err, null, 2)}\`\n`, "error");
}

export const sendLog = (ctx: TelegrafContext, msg: string) => {
    sendToLogChannel(ctx, msg, "log");
}

export const helperCommands = {
    // drawover: async (ctx: TelegrafContext) => {
    //     try {
    //         if (ctx.message?.reply_to_message && "photo" in ctx.message.reply_to_message) {
    //             let photo = ctx.message.reply_to_message.photo[0].file_id;
    //             let msg = await ctx.tg.sendPhoto(process.env.crit_id!, photo, {caption: `By ${getName(ctx.message.reply_to_message.from!)}`});
    //             // let msg = await ctx.tg.forwardMessage(process.env.crit_id!, ctx.message.chat.id, ctx.message.reply_to_message.message_id);

    //             await ctx.reply(`PaintOver/Review for your art: [here](https://t.me/${process.env.crit_username}/${msg.message_id}).`, {
    //                 reply_to_message_id: ctx.message.reply_to_message.message_id,
    //                 parse_mode: "Markdown",
    //                 reply_markup: Markup.inlineKeyboard([
    //                     Markup.callbackButton("Delete the post on @DigitalPaintingReviews", `delme:${ctx.message.reply_to_message.from?.id}:${msg.message_id}`)
    //                 ])
    //             });
    //         } else {
    //             await ctx.replyWithMarkdown(`\`/drawover\` command only works when replied to an image.`, { parse_mode: "Markdown"});
    //         }
    //         await ctx.deleteMessage(ctx.message?.message_id);
    //     } catch (err) {
    //         sendError(ctx, err);
    //     }
    // },
    info: async (ctx: TelegrafContext) => {
        try {
            if (isAdmin(await ctx.getChatAdministrators(), ctx.from?.id)) {
                await ctx.replyWithMarkdown(`\`${JSON.stringify(ctx.message)}\``);
            }
            await ctx.deleteMessage(ctx.message?.message_id);
        } catch (err) {
            sendError(ctx, err);
        }
    },
    help: command('help', data.help),
    start: command('start', data.start),
}

export const utilityFunctions = {
    newChatMember: async (ctx: TelegrafContext) => {
        try {
            await ctx.replyWithHTML(`Welcome <a href="tg://user?id=${ctx.from?.id}">${getName(ctx.from!)}</a>! please read the rules and the pinned message`, {
                disable_web_page_preview: true,
                reply_markup: Markup.inlineKeyboard([
                    (Markup.urlButton('Rules', `${process.env.link_rules}`)),
                    (Markup.urlButton(`Resources`, `${process.env.link_res}`)),
                    (Markup.urlButton('Notes', `${process.env.link_notes}`))])
            }).then((_msg) => {
                sendLog(ctx, `Handled new chat member`);
            });
            await ctx.deleteMessage();
        } catch (err) {
            sendError(ctx, err);
        }
    },
    removedUsers: async (ctx: TelegrafContext) => {
        try {
            await ctx.deleteMessage();
        } catch (err) {
            sendError(ctx, err);
        }
    }
}
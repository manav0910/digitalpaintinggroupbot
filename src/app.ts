import { Composer, Extra} from 'telegraf'
import { sendLog, helperCommands, sendError } from './funcs'

const bot = new Composer();

bot.use(async (ctx, next) => {
    try {
        if (ctx.message?.sender_chat?.type == 'channel') {
            await ctx.deleteMessage();
        }
    }
    catch (err) {
        sendError(ctx, err);
    }

    await next();
});

for (const cmd in helperCommands) {
    bot.command(cmd, (ctx) => {
        Reflect.get(helperCommands, cmd)(ctx);
        sendLog(ctx, `Bot command /${cmd} registered`);
    })
}

// bot.on('new_chat_members', (ctx) => {
//     utilityFunctions.newChatMember(ctx);
// })

// bot.on('left_chat_member', (ctx) => {
//      utilityFunctions.removedUsers(ctx);
// })

// bot.action(/delme:+(\w+):(\w+)/, async (ctx) => {
//     let message_id = ctx.match?.pop();
//     let user_id = ctx.match?.pop();

//     try {
//         if (ctx.from?.id == user_id) {
//             await ctx.editMessageText("The post will de DELETED for ever, Are you sure?", Extra.markdown().markup((m) => {
//                 return m.inlineKeyboard([
//                     m.callbackButton("Yes", `areyousure:${user_id}:${message_id}`),
//                     m.callbackButton("Go Back", `back:${user_id}:${message_id}`)
//                 ]);
//             }));
//         }
//     } catch (err) {
//         sendError(ctx, err)
//     }
// });

// bot.action(/back:+(\w+):(\w+)/, async (ctx) => {
//     let message_id = ctx.match?.pop();
//     let user_id = ctx.match?.pop();

//     try {
//         if (ctx.from?.id == user_id) {
//             await ctx.editMessageText(`PaintOver/Review for your art: [here](https://t.me/${process.env.crit_username}/${message_id}).`, Extra.markdown().markup((m) => {
//                 return m.inlineKeyboard([
//                     m.callbackButton("Delete the post on @DigitalPaintingReviews", `delme:${user_id}:${message_id}`),
//                 ]);
//             }));
//         }
//     } catch (err) {
//         sendError(ctx, err)
//     }
// });

// bot.action(/areyousure:+(\w+):(\w+)/, async (ctx) => {
//     let message_id = ctx.match?.pop();
//     let user_id = ctx.match?.pop();

//     try {
//         if (ctx.from?.id == user_id) {
//             await ctx.editMessageText("ARE YOU REALLY SURE??", Extra.markdown().markup((m) => {
//                 return m.inlineKeyboard([
//                     m.callbackButton("YES", `deleteit:${user_id}:${message_id}`),
//                     m.callbackButton("NO! GO BACK!!", `back:${user_id}:${message_id}`)
//                 ]);
//             }));
//         }
//     } catch (err) {
//         sendError(ctx, err);
//     }
// })

// bot.action(/deleteit:+(\w+):(\w+)/, async (ctx) => {
//     let message_id = ctx.match?.pop();
//     let user_id = ctx.match?.pop();

//     try {
//         if (ctx.from?.id == user_id) {
//             await ctx.deleteMessage(ctx.message?.message_id);
//             await ctx.tg.deleteMessage(`@${process.env.crit_username!}`, Number(message_id));
//         }
//     } catch (err) {
//         sendError(ctx, err);
//     }
// })

module.exports = bot